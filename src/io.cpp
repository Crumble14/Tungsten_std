#include "std.hpp"

void Tungsten_std::n_open(VM&, Thread& thread)
{
	auto& stack = thread.get_stack();
	const auto flag = stack.get<int32_t>();
	const auto path = reinterpret_cast<char*>(stack.get<intptr_t>());

	stack.push<int32_t>(open(path, flag));
}

void Tungsten_std::n_close(VM&, Thread& thread)
{
	auto& stack = thread.get_stack();
	stack.push<int32_t>(close(stack.get<int32_t>()));
}

void Tungsten_std::n_read(VM&, Thread& thread)
{
	auto& stack = thread.get_stack();
	const auto size = stack.get<int32_t>();
	const auto buffer = reinterpret_cast<void*>(stack.get<intptr_t>());
	const auto fd = stack.get<int32_t>();

	stack.push<int32_t>(read(fd, buffer, size));
}

void Tungsten_std::n_write(VM&, Thread& thread)
{
	auto& stack = thread.get_stack();
	const auto size = stack.get<int32_t>();
	const auto buffer = reinterpret_cast<void*>(stack.get<intptr_t>());
	const auto fd = stack.get<int32_t>();

	stack.push<int32_t>(write(fd, buffer, size));
}

void Tungsten_std::n_mkdir(VM&, Thread& thread)
{
	auto& stack = thread.get_stack();
	const auto flag = stack.get<int32_t>();
	const auto path = reinterpret_cast<char*>(stack.get<intptr_t>());

	stack.push<int32_t>(mkdir(path, flag));
}

void Tungsten_std::n_rename(VM&, Thread& thread)
{
	auto& stack = thread.get_stack();
	const auto new_name = reinterpret_cast<char*>(stack.get<intptr_t>());
	const auto old_name = reinterpret_cast<char*>(stack.get<intptr_t>());

	stack.push<int32_t>(rename(old_name, new_name));
}

void Tungsten_std::n_remove(VM&, Thread& thread)
{
	auto& stack = thread.get_stack();
	const auto path = reinterpret_cast<char*>(stack.get<intptr_t>());

	stack.push<int32_t>(remove(path));
}

void Tungsten_std::n_opendir(VM&, Thread& thread)
{
	(void) thread;
	// TODO
}

void Tungsten_std::n_closedir(VM&, Thread& thread)
{
	(void) thread;
	// TODO
}

void Tungsten_std::n_readdir(VM&, Thread& thread)
{
	(void) thread;
	// TODO
}
