#include "std.hpp"

void Tungsten_std::n_sin(VM&, Thread& thread)
{
	auto& stack = thread.get_stack();
	stack.push(sin(stack.get<double>()));
}

void Tungsten_std::n_cos(VM&, Thread& thread)
{
	auto& stack = thread.get_stack();
	stack.push(cos(stack.get<double>()));
}

void Tungsten_std::n_tan(VM&, Thread& thread)
{
	auto& stack = thread.get_stack();
	stack.push(tan(stack.get<double>()));
}

void Tungsten_std::n_asin(VM&, Thread& thread)
{
	auto& stack = thread.get_stack();
	stack.push(asin(stack.get<double>()));
}

void Tungsten_std::n_acos(VM&, Thread& thread)
{
	auto& stack = thread.get_stack();
	stack.push(acos(stack.get<double>()));
}

void Tungsten_std::n_atan(VM&, Thread& thread)
{
	auto& stack = thread.get_stack();
	stack.push(atan(stack.get<double>()));
}

void Tungsten_std::n_exp(VM&, Thread& thread)
{
	auto& stack = thread.get_stack();
	stack.push(exp(stack.get<double>()));
}

void Tungsten_std::n_ln(VM&, Thread& thread)
{
	auto& stack = thread.get_stack();
	stack.push(log2(stack.get<double>()));
}

void Tungsten_std::n_log(VM&, Thread& thread)
{
	auto& stack = thread.get_stack();
	stack.push(log(stack.get<double>()));
}

void Tungsten_std::n_pow(VM&, Thread& thread)
{
	auto& stack = thread.get_stack();
	const auto v1 = stack.get<double>(),
		v2 = stack.get<double>();

	stack.push(pow(v2, v1));
}

void Tungsten_std::n_sqrt(VM&, Thread& thread)
{
	auto& stack = thread.get_stack();
	stack.push(sqrt(stack.get<double>()));
}

void Tungsten_std::n_cbrt(VM&, Thread& thread)
{
	auto& stack = thread.get_stack();
	stack.push(cbrt(stack.get<double>()));
}

void Tungsten_std::n_floor(VM&, Thread& thread)
{
	auto& stack = thread.get_stack();
	stack.push(floor(stack.get<double>()));
}

void Tungsten_std::n_ceil(VM&, Thread& thread)
{
	auto& stack = thread.get_stack();
	stack.push(ceil(stack.get<double>()));
}
