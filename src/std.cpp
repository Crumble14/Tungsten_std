#include "std.hpp"

extern "C" void init(Tungsten::VM& vm)
{
	using namespace Tungsten_std;

	vm.reserve_natives(37);

	vm.register_native("stack_size", n_stack_size);
	vm.register_native("stack_reserve", n_stack_reserve);
	vm.register_native("callstack_depth", n_callstack_depth);
	vm.register_native("callstack_reserve", n_callstack_reserve);
	vm.register_native("malloc", n_malloc);
	vm.register_native("realloc", n_realloc);
	vm.register_native("free", n_free);

	vm.register_native("loadLibrary", n_load_library);

	vm.register_native("open", n_open);
	vm.register_native("close", n_close);
	vm.register_native("read", n_read);
	vm.register_native("write", n_write);
	vm.register_native("mkdir", n_mkdir);
	vm.register_native("rename", n_rename);
	vm.register_native("remove", n_remove);
	vm.register_native("opendir", n_opendir);
	vm.register_native("closedir", n_closedir);
	vm.register_native("readdir", n_readdir);

	vm.register_native("sin", n_sin);
	vm.register_native("cos", n_cos);
	vm.register_native("tan", n_tan);
	vm.register_native("asin", n_asin);
	vm.register_native("acos", n_acos);
	vm.register_native("atan", n_atan);
	vm.register_native("exp", n_exp);
	vm.register_native("ln", n_ln);
	vm.register_native("log", n_log);
	vm.register_native("pow", n_pow);
	vm.register_native("sqrt", n_sqrt);
	vm.register_native("cbrt", n_cbrt);
	vm.register_native("floor", n_floor);
	vm.register_native("ceil", n_ceil);

	vm.register_native("thread_get_id", n_thread_get_id);
	vm.register_native("thread_create", n_thread_create);
	vm.register_native("thread_sleep", n_thread_sleep);
	vm.register_native("thread_exit", n_thread_exit);
	vm.register_native("thread_kill", n_thread_kill);
}
