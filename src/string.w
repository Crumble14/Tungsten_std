@include "memory.w"

@define STREND	'\0'

namespace std
{
	uint32 strlen(const int8* str)
	{
		uint32 len = 0

		while(str[len] != $STREND) {
			++len
		}

		return (len)
	}

	int8* strcpy(int8* dest, const int8* src)
	{
		if(dest == $NULL | src == $NULL) {
			return (dest)
		}

		return (memcpy(dest, src, strlen(src)))
	}

	int8* strcat(int8* dest, const int8* src)
	{
		if(dest == $NULL | src == $NULL) {
			return (dest)
		}

		return (memcpy(dest + strlen(dest), src, strlen(src)))
	}

	int8* strdup(const int8* str)
	{
		const uint32 len = strlen(str)

		int8* s = malloc(len + 1)

		if(!s) {
			return (0)
		}

		memcpy(s, str, len)
		return (s)
	}
}
