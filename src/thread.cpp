#include "std.hpp"

using namespace Tungsten_std;

struct ThreadInfo
{
	VM* vm;

	uint32_t begin;
	void* param;
};

void* create_thread(void* info)
{
	const auto i = (ThreadInfo*) info;
	auto vm = i->vm;
	auto begin = i->begin;
	auto param = i->param;

	delete i;

	Thread thread(pthread_self());
	thread.get_stack().push(param);
	vm->run(begin, thread);

	return nullptr;
}

void Tungsten_std::n_thread_get_id(VM&, Thread& thread)
{
	thread.get_stack().push<uint32_t>(thread.get_id());
}

void Tungsten_std::n_thread_create(VM& vm, Thread& thread)
{
	auto& stack = thread.get_stack();
	const auto parameter = stack.get<void*>();
	const auto begin = stack.get<uint32_t>();

	ThreadInfo* info = new ThreadInfo();
	info->vm = &vm;
	info->begin = begin;
	info->param = parameter;

	pthread_t t;
	pthread_create(&t, nullptr, create_thread, info);

	// TODO Detach?
}

void Tungsten_std::n_thread_sleep(VM&, Thread& thread)
{
	auto& stack = thread.get_stack();
	const auto delay = stack.get<uint32_t>();

	this_thread::sleep_for(chrono::milliseconds(delay));
}

void Tungsten_std::n_thread_exit(VM&, Thread& thread)
{
	pthread_exit(nullptr);
	thread.destroy();
}

void Tungsten_std::n_thread_kill(VM&, Thread& thread)
{
	auto& stack = thread.get_stack();
	const auto t = stack.get<uint64_t>();

	pthread_kill(t, SIGKILL);
}
